package io.powerledger.powersource.services;

import io.powerledger.powersource.commons.dtos.base.BaseResponseDTO;
import io.powerledger.powersource.commons.dtos.request.PowerSourceRequestDTO;
import io.powerledger.powersource.commons.dtos.response.PowerSourcePostCodeFilterResponseDTO;

import java.util.List;


public interface PowerSourceService {
    BaseResponseDTO<Object> createPowerSource(List<PowerSourceRequestDTO> requestDTOS);
    BaseResponseDTO<PowerSourcePostCodeFilterResponseDTO> fetchNamesListDTOByPostcodeRange(int start, int end);
}
