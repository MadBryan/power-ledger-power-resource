package io.powerledger.powersource.services.impls;

import io.powerledger.powersource.commons.dtos.base.BaseResponseDTO;
import io.powerledger.powersource.commons.dtos.request.PowerSourceRequestDTO;
import io.powerledger.powersource.commons.dtos.response.PowerSourcePostCodeFilterResponseDTO;
import io.powerledger.powersource.commons.utils.MapperUtil;
import io.powerledger.powersource.entities.PowerSource;
import io.powerledger.powersource.repositories.PowerSourceRepository;
import io.powerledger.powersource.services.PowerSourceService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static io.powerledger.powersource.commons.constants.Constants.*;

@Service
@Log4j2
@RequiredArgsConstructor
public class PowerSourceServiceImpl implements PowerSourceService {

    @NonNull
    private final PowerSourceRepository powerSourceRepository;

    /**
     * Save list of name postcode dt os iterable.
     *
     * @param requestDTOS the powersource dt os
     * @return the iterable
     */
    @Transactional(rollbackFor = Exception.class)
    public BaseResponseDTO<Object> createPowerSource(List<PowerSourceRequestDTO> requestDTOS) {
        powerSourceRepository.saveAll(requestDTOS
                .stream()
                .map(MapperUtil::toEntity)
                .collect(Collectors.toList())
        );
        return BaseResponseDTO.builder()
                .code(CREATED)
                .message(STORE_POWERSOURCE_SUCCESS_MESSAGE)
                .build();
    }

    /**
     * Fetch powersource list dto by postcode range powersource list dto.
     *
     * @param start the start
     * @param end   the end
     * @return the powersource list dto
     */
    @Transactional(readOnly = true)
    public BaseResponseDTO<PowerSourcePostCodeFilterResponseDTO> fetchNamesListDTOByPostcodeRange(int start,int end) {
        List<PowerSource> powerSourceList = powerSourceRepository.findByPostcodeBetween(start,end);
        List<String> nameList = powerSourceList.stream()
                .sorted(Comparator.comparing(PowerSource::getName))
                .map(PowerSource::getName)
                .collect(Collectors.toList());

        int totalWattCapacity = powerSourceList.stream()
                .map(PowerSource::getWattCapacity)
                .reduce(0,Integer::sum);
        float averageWattCapacity = 0.0f;
        if (powerSourceList.size() > 0) {
            averageWattCapacity = (float) totalWattCapacity / powerSourceList.size();
        }
        return BaseResponseDTO.<PowerSourcePostCodeFilterResponseDTO>builder()
                .code(OK)
                .message(FILTER_BY_POSTCODE_SUCCESS_MESSAGE)
                .data(new PowerSourcePostCodeFilterResponseDTO(nameList,totalWattCapacity,averageWattCapacity))
                .build();
    }
}
