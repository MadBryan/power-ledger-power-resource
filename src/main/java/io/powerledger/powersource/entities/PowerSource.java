package io.powerledger.powersource.entities;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "powersource",
        uniqueConstraints = @UniqueConstraint(name = "uniqueNameAndPostcode", columnNames = {"name", "postcode"}
        ))
public class PowerSource {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Integer postcode;

    @Column(nullable = false)
    private Integer wattCapacity;
}
