package io.powerledger.powersource.controllers;

import io.powerledger.powersource.commons.dtos.base.BaseResponseDTO;
import io.powerledger.powersource.commons.dtos.request.PowerSourceRequestDTO;
import io.powerledger.powersource.commons.dtos.response.PowerSourcePostCodeFilterResponseDTO;
import io.powerledger.powersource.services.PowerSourceService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

import static io.powerledger.powersource.commons.constants.Constants.*;

@RestController
@Validated
@RequiredArgsConstructor
@Log4j2
@RequestMapping(("/api/v1/powersource"))
public class PowerSourceController {

    @NonNull
    private final PowerSourceService powerSourceService;

    /**
     * Store list of postcode names response entity.
     *
     * @param requestDTOS the postcode dt os
     * @return the response entity
     */
    @PostMapping
    public ResponseEntity<BaseResponseDTO<Object>> storeListOfPostcodeNames(@RequestBody List<@Valid PowerSourceRequestDTO> requestDTOS) {
        logger.debug(POSTCODE_STORE_REQUEST_DEBUG,() -> requestDTOS);
        powerSourceService.createPowerSource(requestDTOS);
        return new ResponseEntity<>(BaseResponseDTO
                .builder()
                .code(CREATED)
                .message(STORE_POWERSOURCE_SUCCESS_MESSAGE)
                .build()
                ,HttpStatus.CREATED);
    }

    /**
     * Fetch powersource list dto by postcode range powersource list dto.
     *
     * @param postcodeStart the postcode start
     * @param postcodeEnd   the postcode end
     * @return the powersource list dto
     */
    @GetMapping
    public ResponseEntity<BaseResponseDTO<PowerSourcePostCodeFilterResponseDTO>> fetchNamesListByPostcodeRange(
            @RequestParam(value = "postcodeStart")
            @Min(value = 200, message = POSTCODE_RANGE_MESSAGE)
            @Max(value = 9999, message = POSTCODE_RANGE_MESSAGE) int postcodeStart,
            @RequestParam(value = "postcodeEnd")
            @Min(value = 200, message = POSTCODE_RANGE_MESSAGE)
            @Max(value = 9999, message = POSTCODE_RANGE_MESSAGE) int postcodeEnd) {
        logger.debug(FETCH_NAME_BY_POSTCODE_REQUEST_DEBUG,() -> postcodeStart,() -> postcodeEnd);
        BaseResponseDTO<PowerSourcePostCodeFilterResponseDTO> responseBody =
                powerSourceService.fetchNamesListDTOByPostcodeRange(postcodeStart,postcodeEnd);
        logger.trace(FETCH_NAME_BY_POSTCODE_RESPONSE_DEBUG,() -> responseBody,() -> postcodeStart,() -> postcodeEnd);
        return new ResponseEntity<>(responseBody,HttpStatus.OK);
    }
}
