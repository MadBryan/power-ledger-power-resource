package io.powerledger.powersource.commons.constants;

public @interface Constants {
    //Message and Debug
    public static final String POSTCODE_RANGE_MESSAGE = "postcode must be between 200 & 9999";
    public static final String POSTCODE_STORE_REQUEST_DEBUG = "Received request to store postcode names: {}";
    public static final String FETCH_NAME_BY_POSTCODE_REQUEST_DEBUG = "Received request to fetch names by postcodes from {} to {}";
    public static final String FETCH_NAME_BY_POSTCODE_RESPONSE_DEBUG = "Response result {} for postcodes from {} to {}";
    public static final String LOG_WARNING = "Caught Exception: {}, Request: {}";
    public static final String LOG_ERROR = "Caught Exception: {}, Request: {}";
    //Message Code
    public static final Integer OK = 200;
    public static final Integer CREATED = 201;
    public static final Integer BAD_REQUEST = 400;
    public static final Integer SERVER_INTERNAL_ERROR = 500;
    public static final String STORE_POWERSOURCE_SUCCESS_MESSAGE = "Stored power source successfully";
    public static final String FILTER_BY_POSTCODE_SUCCESS_MESSAGE = "Get result successfully";
}
