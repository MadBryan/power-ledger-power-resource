package io.powerledger.powersource.commons.dtos.response;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
public class PowerSourcePostCodeFilterResponseDTO {

    List<String> names;
    int totalWattCapacity;
    float averageWattCapacity;
}