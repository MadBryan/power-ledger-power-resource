package io.powerledger.powersource.commons.dtos.request;

import lombok.Value;

import javax.validation.constraints.*;

@Value
public class PowerSourceRequestDTO {
    @NotBlank
    @Size(min = 2, max = 32, message = "name must be between 2 and 32 characters long")
    String name;

    @NotNull
    @Min(value = 200, message = "postcode must be between 200 & 9999")
    @Max(value = 9999, message = "postcode must be between 200 & 9999")
    int postcode;

    @NotNull
    int wattCapacity;
}
