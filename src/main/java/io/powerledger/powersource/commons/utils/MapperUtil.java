package io.powerledger.powersource.commons.utils;

import io.powerledger.powersource.commons.dtos.request.PowerSourceRequestDTO;
import io.powerledger.powersource.entities.PowerSource;

public class MapperUtil {
    public static PowerSource toEntity(PowerSourceRequestDTO dto) {
        return PowerSource.builder()
                .name(dto.getName())
                .postcode(dto.getPostcode())
                .wattCapacity(dto.getWattCapacity())
                .build();
    }
}
