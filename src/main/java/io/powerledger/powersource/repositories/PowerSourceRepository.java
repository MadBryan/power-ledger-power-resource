package io.powerledger.powersource.repositories;

import io.powerledger.powersource.entities.PowerSource;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PowerSourceRepository extends CrudRepository<PowerSource, Integer> {
    /**
     * Find battery by postcode between list.
     *
     * @param min the min
     * @param max the max
     * @return the list
     */
    List<PowerSource> findByPostcodeBetween(int min, int max);
}
