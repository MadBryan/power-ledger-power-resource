package io.powerledger.powersource.commons.utils;

import io.powerledger.powersource.commons.dtos.request.PowerSourceRequestDTO;
import io.powerledger.powersource.entities.PowerSource;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MapperUtilTest {
    @Test
    public void shouldMappedToEntityCorrectly() {
        PowerSourceRequestDTO dto = new PowerSourceRequestDTO("Xiaomi", 6158, 150);
        PowerSource entity = MapperUtil.toEntity(dto);

        assertThat(entity.getName()).isEqualTo("Xiaomi");
        assertThat(entity.getPostcode()).isEqualTo(6158);
        assertThat(entity.getWattCapacity()).isEqualTo(150);
        assertThat(entity.getId()).isNull();
    }
}
