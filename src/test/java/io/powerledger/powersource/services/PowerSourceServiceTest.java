package io.powerledger.powersource.services;

import io.powerledger.powersource.commons.dtos.base.BaseResponseDTO;
import io.powerledger.powersource.commons.dtos.request.PowerSourceRequestDTO;
import io.powerledger.powersource.commons.dtos.response.PowerSourcePostCodeFilterResponseDTO;
import io.powerledger.powersource.entities.PowerSource;
import io.powerledger.powersource.repositories.PowerSourceRepository;
import io.powerledger.powersource.services.impls.PowerSourceServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = {PowerSourceServiceImpl.class})
public class PowerSourceServiceTest {

    @Captor
    public ArgumentCaptor<List<PowerSource>> listArgumentCaptor;
    @MockBean
    private PowerSourceRepository repository;
    @Autowired
    private PowerSourceService powerSourceService;

    @AfterEach
    public void teardown() {
        Mockito.reset(repository);
    }

    @Test
    public void shouldStoreEmptyList() {
        powerSourceService.createPowerSource(new ArrayList<>());
        verify(repository,times(1)).saveAll(listArgumentCaptor.capture());

        List<PowerSource> capturedList = listArgumentCaptor.getValue();
        assertThat(capturedList).isEmpty();
    }

    @Test
    public void shouldStoreListOfNamePostcodeDTOs() {
        powerSourceService.createPowerSource(new ArrayList<>(Arrays.asList(
                new PowerSourceRequestDTO("Aukey",7899,100),
                new PowerSourceRequestDTO("Tesla Model X",2203,150))));

        verify(repository,times(1)).saveAll(listArgumentCaptor.capture());

        List<PowerSource> capturedList = listArgumentCaptor.getValue();
        assertThat(capturedList).hasSize(2)
                .extracting(PowerSource::getName).containsExactly("Aukey","Tesla Model X");
        assertThat(capturedList).extracting(PowerSource::getPostcode).containsExactly(7899,2203);
    }

    @Test
    public void shouldReturnValueOfWattCapacity() {
        doReturn(new ArrayList<>(Arrays.asList(
                new PowerSource(1,"Anker",2000,150),
                new PowerSource(2,"Aukey",2570,100),
                new PowerSource(3,"Energizer",4000,50))))
                .when(repository)
                .findByPostcodeBetween(eq(2000),eq(4500));

        BaseResponseDTO<PowerSourcePostCodeFilterResponseDTO> responseList =
                powerSourceService.fetchNamesListDTOByPostcodeRange(2000,4500);

        assertThat(responseList.getData().getAverageWattCapacity()).isEqualTo(100.0f);
        assertThat(responseList.getData().getTotalWattCapacity()).isEqualTo(300);

    }
}
