package io.powerledger.powersource.controllers;

import io.powerledger.powersource.commons.dtos.base.BaseResponseDTO;
import io.powerledger.powersource.commons.dtos.request.PowerSourceRequestDTO;
import io.powerledger.powersource.commons.dtos.response.PowerSourcePostCodeFilterResponseDTO;
import io.powerledger.powersource.services.PowerSourceService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PowerSourceController.class)
public class PowerSourceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PowerSourceService powerSourceService;

    @Captor
    private ArgumentCaptor<List<PowerSourceRequestDTO>> powerSourceDTOListCaptor;

    @Test
    public void shouldStoreEmptyArray() throws Exception {
        MockHttpServletRequestBuilder request = post("/api/v1/powersource")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isCreated());

        verify(powerSourceService,times(1)).createPowerSource(powerSourceDTOListCaptor.capture());
        List<PowerSourceRequestDTO> capturedInput = powerSourceDTOListCaptor.getValue();
        assertThat(capturedInput).isEmpty();
    }

    @Test
    public void shouldStoreArrayOfNames() throws Exception {
        MockHttpServletRequestBuilder request = post("/api/v1/powersource")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"Anker\", \"postcode\": 1000, \"wattCapacity\": 150 }, { \"name\": " +
                        "\"Energizer\", \"postcode\": 9999, \"wattCapacity\": 150 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isCreated());

        verify(powerSourceService,times(1)).createPowerSource(powerSourceDTOListCaptor.capture());
        List<PowerSourceRequestDTO> capturedInput = powerSourceDTOListCaptor.getValue();
        assertThat(capturedInput).hasSize(2)
                .extracting(PowerSourceRequestDTO::getName).containsExactly("Anker","Energizer");
        assertThat(capturedInput)
                .extracting(PowerSourceRequestDTO::getPostcode).containsExactly(1000,9999);
        assertThat(capturedInput)
                .extracting(PowerSourceRequestDTO::getWattCapacity).containsExactly(150,150);
    }

    @Test
    public void shouldErrorWithMissingName() throws Exception {
        MockHttpServletRequestBuilder request = post("/api/v1/powersource")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"postcode\": 6000 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("must not be blank")));

        verify(powerSourceService,times(0)).createPowerSource(anyList());
    }

    @Test
    public void shouldErrorWithNullName() throws Exception {
        MockHttpServletRequestBuilder request = post("/api/v1/powersource")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": null, \"postcode\": 6000, \"wattCapacity\": 150 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("must not be blank")));

        verify(powerSourceService,times(0)).createPowerSource(anyList());
    }

    @Test
    public void shouldErrorWithEmptyName() throws Exception {
        MockHttpServletRequestBuilder request = post("/api/v1/powersource")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"\", \"postcode\": 6000,  \"wattCapacity\": 6000 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("must not be blank")));

        verify(powerSourceService,times(0)).createPowerSource(anyList());
    }

    @Test
    public void shouldErrorWithTooShortName() throws Exception {
        MockHttpServletRequestBuilder request = post("/api/v1/powersource")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"c\", \"postcode\": 6000,  \"wattCapacity\": 150 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("name must be between 2 and 32 characters long")));

        verify(powerSourceService,times(0)).createPowerSource(anyList());
    }

    @Test
    public void shouldErrorWithTooLongName() throws Exception {
        MockHttpServletRequestBuilder request = post("/api/v1/powersource")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"dhfjrnghtidjskwifoentorowlsmcjfod\", \"postcode\": 6000 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("name must be between 2 and 32 characters long")));

        verify(powerSourceService,times(0)).createPowerSource(anyList());
    }

    @Test
    public void shouldErrorWithMissingPostcode() throws Exception {
        MockHttpServletRequestBuilder request = post("/api/v1/powersource")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"Anker\" }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));

        verify(powerSourceService,times(0)).createPowerSource(anyList());
    }

    @Test
    public void shouldErrorWithPostcodeBelowRange() throws Exception {
        MockHttpServletRequestBuilder request = post("/api/v1/powersource")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"Anker\", \"postcode\": 199, \"wattCapacity\": 150 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));

        verify(powerSourceService,times(0)).createPowerSource(anyList());
    }

    @Test
    public void shouldErrorWithPostcodeAboveRange() throws Exception {
        MockHttpServletRequestBuilder request = post("/api/v1/powersource")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"Anker\", \"postcode\": 10000 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));

        verify(powerSourceService,times(0)).createPowerSource(anyList());
    }

    @Test
    public void shouldErrorWithInvalidPostcodeType() throws Exception {
        MockHttpServletRequestBuilder request = post("/api/v1/powersource")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"Anker\", \"postcode\": \"Cheeky\" }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("JSON parse error: Cannot deserialize value of type `int`")));

        verify(powerSourceService,times(0)).createPowerSource(anyList());
    }

    @Test
    public void shouldFetchWithEmptyResponse() throws Exception {
        doReturn(new BaseResponseDTO<>(200,"success",new PowerSourcePostCodeFilterResponseDTO(new ArrayList<String>()
                ,0,(float) 0.0)))
                .when(powerSourceService)
                .fetchNamesListDTOByPostcodeRange(eq(201),eq(9998));

        MockHttpServletRequestBuilder request = get("/api/v1/powersource?postcodeStart=201&postcodeEnd=9998")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"names\":[],\"totalWattCapacity\":0," +
                        "\"averageWattCapacity\":0.0}")));

        verify(powerSourceService,times(1)).fetchNamesListDTOByPostcodeRange(201,9998);
    }

    @Test
    public void shouldErrorWithLowerBoundStart() throws Exception {
        MockHttpServletRequestBuilder request = get("/api/v1/powersource?postcodeStart=199&postcodeEnd=9999")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));
    }

    @Test
    public void shouldErrorWithLUpperBoundStart() throws Exception {
        MockHttpServletRequestBuilder request = get("/api/v1/powersource?postcodeStart=10000&postcodeEnd=9999")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));
    }

    @Test
    public void shouldErrorWithLowerBoundEnd() throws Exception {
        MockHttpServletRequestBuilder request = get("/api/v1/powersource?postcodeStart=200&postcodeEnd=199")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));
    }

    @Test
    public void shouldErrorWithLUpperBoundEnd() throws Exception {
        MockHttpServletRequestBuilder request = get("/api/v1/powersource?postcodeStart=200&postcodeEnd=10000")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));
    }
}
