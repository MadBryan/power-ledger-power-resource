FROM openjdk:11
WORKDIR /
ADD target/powersource-0.0.1-SNAPSHOT.jar powersource.jar
EXPOSE 8080
CMD java -jar powersource.jar