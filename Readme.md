# PowerSource Storing Application

A simple rest service that allows the storage of **PowerSources** along with total and average watt capacity.

## SWAGGER

SWAGGER UI: http://localhost:8080/swagger-ui.html

SWAGGER API docs: http://localhost:8080/v2/api-docs

## Storing PowerSource with Name, Postcode and Watt Capacity

You can store names & postcodes into this service by sending a `POST` request to `/api/v1/powersource` of the server. The format should be in a JSON array with the child object's containing the respective **name** and **postcode** attributes. For example:
```sh
curl --location --request POST 'localhost:8080/api/v1/powersource' \
--header 'Content-Type: application/json' \
--data-raw '[
{ "name": "Hawker", "postcode": 4256, "wattCapacity": 150 },
{ "name": "Power Wheels", "postcode": 6245, "wattCapacity": 100 },
{ "name": "Energizer", "postcode": 6354, "wattCapacity": 120 },
{ "name": "Aukey", "postcode": 6875, "wattCapacity": 50 }
]
```
Sample JSON input:
```json
[
      { "name": "Hawker", "postcode": 4256, "wattCapacity": 150 },
      { "name": "Power Wheels", "postcode": 6245, "wattCapacity": 100 },
      { "name": "Energizer", "postcode": 6354, "wattCapacity": 120 },
      { "name": "Aukey", "postcode": 6875, "wattCapacity": 50 }
]
```
PowerSource names are also to be between 2 and 32 characters long.

This should return for the prior dataset
```json
{
    "code": 201,
    "message": "Stored power source successfully",
    "data": null
}
```
## Fetching PowerSources information by Postcode Range

The PowerSources names, total watt capacity and average watt capacity can be retrieved by `GET` request to the `/api/v1/powersource` endpoint of the server along with a filtered postcode range placed in the `postcodeStart` and `postcodeEnd` query parameters.

```sh
$curl -L -X GET 'localhost:8080/api/v1/powersource?postcodeStart=6321&postcodeEnd=6897'
```

This should return for the prior dataset

```json
{
  "code": 200,
  "message": "Get result successfully",
  "data": {
    "names": [
      "Aukey",
      "Energizer"
    ],
    "totalWattCapacity": 170,
    "averageWattCapacity": 85.0
  }
}
```